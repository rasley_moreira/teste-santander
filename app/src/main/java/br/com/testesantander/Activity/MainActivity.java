package br.com.testesantander.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

import br.com.testesantander.R;
import br.com.testesantander.Presentation.Enums.EFragmentManager;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private static MainActivity _instance;

    public static MainActivity getInstance() {
        if(MainActivity._instance == null)
            return new MainActivity();

        return MainActivity._instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        this.init();
    }

    private void init() {
        MainActivity._instance = this;
        MainActivity.fragmentManager(EFragmentManager.LOGIN);
    }

    public static void fragmentManager(EFragmentManager fragment){
        try {
            FragmentTransaction transaction = MainActivity.getInstance().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainContainer, fragment.getValue());
            transaction.commit();
        }catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
    }

}
