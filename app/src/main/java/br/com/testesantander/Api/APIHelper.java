package br.com.testesantander.Api;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIHelper {

    public static final String API_BASE_URL = "https://bank-app-test.herokuapp.com/api/";

    public static Retrofit createRetrofit(){
        OkHttpClient httpClient = new OkHttpClient().newBuilder().addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder()
                    .addHeader("Content-Type", "application/json");
            Request newRequest = builder.build();
            return chain.proceed(newRequest);
        }).build();

        return new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient)
                .build();
    }

    public static <T> T createService(Class<T> serviceClass) {
        Retrofit retrofit = createRetrofit();
        return retrofit.create(serviceClass);
    }

}
