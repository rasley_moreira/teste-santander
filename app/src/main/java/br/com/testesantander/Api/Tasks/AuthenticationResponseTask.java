package br.com.testesantander.Api.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Objects;

import br.com.testesantander.Api.APIHelper;
import br.com.testesantander.Model.User.User;
import br.com.testesantander.Model.APIResponse.UserResponse;
import br.com.testesantander.Protocols.IAuthentication;
import br.com.testesantander.Protocols.IResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import br.com.testesantander.Model.APIResponse.Error;

public class AuthenticationResponseTask extends AsyncTask<String, Call<UserResponse>, Call<UserResponse>> {

    private static final String TAG = AuthenticationResponseTask.class.getName();

    private Context         context;
    private IResponse       response;
    private KProgressHUD    hud;

    public AuthenticationResponseTask(Context context, IResponse iResponse){
        this.context    = context;
        this.response   = iResponse;
    }

    @Override
    protected void onPreExecute() {
        try {
            this.hud = KProgressHUD.create(this.context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por favor, espere.")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
            this.hud.show();
        }catch (Exception e){
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
        }
    }

    @Override
    protected Call doInBackground(String... objects) {
        try {
            IAuthentication iAutenticacion  = APIHelper.createService(IAuthentication.class);
            return iAutenticacion.requestLogin(objects[0], objects[1]);
        } catch (Exception e) {
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
        }
        return null;
    }

    @Override
    protected void onPostExecute(Call<UserResponse> response) {
        if(response != null){
            response.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getError() != null && response.body().getError().getCode() != 0) {
                            Error error = response.body().getError();
                            AuthenticationResponseTask.this.response.onError(error);
                        } else {
                            User user = response.body().getUserAccount();
                            AuthenticationResponseTask.this.response.onSuccess(user);
                        }
                    }
                    AuthenticationResponseTask.this.hud.dismiss();
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    Log.e(TAG, Objects.requireNonNull(t.getMessage()));
                    Error error = new Error();
                    error.setMessage(t.getMessage());
                    error.setCode(-1);

                    AuthenticationResponseTask.this.response.onFailure(error);
                    AuthenticationResponseTask.this.hud.dismiss();
                }
            });
        }else{
            AuthenticationResponseTask.this.hud.dismiss();
        }
    }
}
