package br.com.testesantander.Api.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import br.com.testesantander.Api.APIHelper;
import br.com.testesantander.Model.APIResponse.Error;
import br.com.testesantander.Model.APIResponse.StatementResponse;
import br.com.testesantander.Model.Statement.Statement;
import br.com.testesantander.Model.User.User;
import br.com.testesantander.Protocols.IResponse;
import br.com.testesantander.Protocols.IStatement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatementResponseTask extends AsyncTask<Void, Call<StatementResponse>, Call<StatementResponse>> {

    private static final String TAG = StatementResponseTask.class.getName();

    private Context                     context;
    private IResponse<List<Statement>>  response;
    private KProgressHUD                hud;
    private User                        user;

    public StatementResponseTask(Context context, IResponse<List<Statement>> iResponse, User user){
        this.context    = context;
        this.response   = iResponse;
        this.user       = user;
    }

    @Override
    protected void onPreExecute() {
        try {
            this.hud = KProgressHUD.create(this.context)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por favor, espere.")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
            this.hud.show();
        }catch (Exception e){
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
        }
    }

    @Override
    protected Call doInBackground(Void... _void) {
        try {
            IStatement iStatement  = APIHelper.createService(IStatement.class);
            return iStatement.requestStatements(this.user.getUserId());
        } catch (Exception e) {
            Log.e(TAG, Objects.requireNonNull(e.getMessage()));
        }
        return null;
    }

    @Override
    protected void onPostExecute(Call<StatementResponse> response) {
        if(response != null){
            response.enqueue(new Callback<StatementResponse>() {
                @Override
                public void onResponse(Call<StatementResponse> call, Response<StatementResponse> response) {
                    if (response.isSuccessful()) {;
                        StatementResponseTask.this.response.onSuccess(response.body().getStatementList());
                    }else{
                        Error error = new Error();
                        try{
                            Gson gson = new Gson();
                            assert response.errorBody() != null;
                            error = gson.fromJson(response.errorBody().string(), Error.class);
                        }catch (IOException e){
                            error.setMessage("Erro desconhecido, ou erro com as credenciais");
                        }

                        StatementResponseTask.this.response.onError(error);
                    }
                    StatementResponseTask.this.hud.dismiss();
                }

                @Override
                public void onFailure(Call<StatementResponse> call, Throwable t) {
                    Log.e(TAG, Objects.requireNonNull(t.getMessage()));
                    Error error = new Error();
                    error.setMessage(t.getMessage());
                    error.setCode(-1);

                    StatementResponseTask.this.response.onFailure(error);
                    StatementResponseTask.this.hud.dismiss();
                }
            });
        }else{
            StatementResponseTask.this.hud.dismiss();
        }
    }
}
