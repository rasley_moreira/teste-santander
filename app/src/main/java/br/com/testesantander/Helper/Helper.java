package br.com.testesantander.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import com.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;

import br.com.testesantander.Model.User.User;
import br.com.testesantander.Protocols.IDialogAction;
import br.com.testesantander.R;

import static android.content.Context.MODE_PRIVATE;

public class Helper {

    private static final String KEY_USER_SESSION_PREFERENCES  = "USER_SESSION_PREFERENCES";
    public static final String  STRING_DATE_INPUT_PATTERN     = "yyyy-MM-dd";
    public static final String  STRING_DATE_OUTPUT_PATTERN    = "dd/MM/yyyy";

    public static void saveUserSession(Context context, User user){
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_USER_SESSION_PREFERENCES, MODE_PRIVATE).edit();
        editor.putInt("user_id", user.getUserId());
        editor.putString("user_name", user.getName());
        editor.putString("user_agency", user.getAgency());
        editor.putString("user_back_account", user.getBankAccount());
        editor.putFloat("user_balance", user.getBalance().floatValue());
        editor.apply();
    }

    public static User getUserSession(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_USER_SESSION_PREFERENCES, MODE_PRIVATE);

        User user = new User();
        user.setUserId(sharedPreferences.getInt("user_id", 0));
        user.setName(sharedPreferences.getString("user_name", ""));
        user.setAgency(sharedPreferences.getString("user_agency", ""));
        user.setBankAccount(sharedPreferences.getString("user_back_account", ""));
        user.setBalance(((Float) sharedPreferences.getFloat("user_balance", 0.0f)).doubleValue());

        return user.getUserId() == 0 ? null : user;
    }

    public static void clearSession(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY_USER_SESSION_PREFERENCES, MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();

        context.deleteSharedPreferences(KEY_USER_SESSION_PREFERENCES);
    }

    public static String simpleMaskAccount(String bankAccount){
        if(bankAccount != null && !bankAccount.equals("")) {
            StringBuilder result = new StringBuilder(bankAccount);
            result.insert(2, ".");
            result.insert(bankAccount.length(), "-");
            return result.toString();
        }

        return bankAccount;
    }

    public static void showDialog(Activity context, String title, String message){
        BottomSheetMaterialDialog mBottomSheetDialog = new BottomSheetMaterialDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK", (dialogInterface, which) -> {
                    dialogInterface.dismiss();
                }).setNegativeButton("Fechar", (dialogInterface, which) -> {
                    dialogInterface.dismiss();
                })
                .build();
        mBottomSheetDialog.show();
    }

    public static void showDialog(Activity context, final IDialogAction iDialogAction, String title, String message){
        BottomSheetMaterialDialog mBottomSheetDialog = new BottomSheetMaterialDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.dialog_button_positive), (dialogInterface, which) -> {
                    iDialogAction.onClickPositiveDialogButton();
                    dialogInterface.dismiss();
                }).setNegativeButton(context.getString(R.string.dialog_button_negative), (dialogInterface, which) -> {
                    iDialogAction.onClickNegativeDialogButton();
                    dialogInterface.dismiss();
                })
                .build();
        mBottomSheetDialog.show();
    }

}
