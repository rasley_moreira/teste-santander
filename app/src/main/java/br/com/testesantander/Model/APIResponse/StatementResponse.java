package br.com.testesantander.Model.APIResponse;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import br.com.testesantander.Model.Statement.Statement;

public class StatementResponse implements Serializable {

    private List<Statement> statementList;
    private Error           error;

    public List<Statement> getStatementList() {
        return statementList;
    }

    public void setStatementList(List<Statement> statementList) {
        this.statementList = statementList;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatementResponse that = (StatementResponse) o;
        return statementList.equals(that.statementList) &&
                error.equals(that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statementList, error);
    }

    @Override
    public String toString() {
        return "StatementResponse{" +
                "statementList=" + statementList +
                ", error=" + error +
                '}';
    }
}
