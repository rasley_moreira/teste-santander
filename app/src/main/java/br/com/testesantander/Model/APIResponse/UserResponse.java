package br.com.testesantander.Model.APIResponse;

import java.io.Serializable;
import java.util.Objects;

import br.com.testesantander.Model.User.User;

public class UserResponse implements Serializable {

    private User    userAccount;
    private Error error;

    public User getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(User userAccount) {
        this.userAccount = userAccount;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return userAccount.equals(that.userAccount) &&
                error.equals(that.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userAccount, error);
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "userAccount=" + userAccount +
                ", error=" + error +
                '}';
    }
}
