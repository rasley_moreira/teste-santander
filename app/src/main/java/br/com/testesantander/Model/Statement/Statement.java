package br.com.testesantander.Model.Statement;

import java.io.Serializable;
import java.util.Objects;

public class Statement implements Serializable {

    private String title;
    private String desc;
    private String date;
    private double value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statement statement = (Statement) o;
        return Double.compare(statement.value, value) == 0 &&
                title.equals(statement.title) &&
                desc.equals(statement.desc) &&
                date.equals(statement.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, desc, date, value);
    }

    @Override
    public String toString() {
        return "Statement{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", date='" + date + '\'' +
                ", value=" + value +
                '}';
    }
}
