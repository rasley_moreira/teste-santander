package br.com.testesantander.Model.User;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

   private int      userId;
   private String   name;
   private String   bankAccount;
   private String   agency;
   private Double   balance;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId &&
                Double.compare(user.balance, balance) == 0 &&
                name.equals(user.name) &&
                bankAccount.equals(user.bankAccount) &&
                agency.equals(user.agency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, name, bankAccount, agency, balance);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", bankAccount='" + bankAccount + '\'' +
                ", agency='" + agency + '\'' +
                ", balance=" + balance +
                '}';
    }
}
