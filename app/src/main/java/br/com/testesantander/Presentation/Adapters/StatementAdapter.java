package br.com.testesantander.Presentation.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

import br.com.testesantander.Helper.Helper;
import br.com.testesantander.Model.Statement.Statement;
import br.com.testesantander.R;

public class StatementAdapter extends RecyclerView.Adapter {

    private static final String TAG = StatementAdapter.class.getName();

    private List<Statement> statements;
    private Context         context;

    public StatementAdapter(Context context, List<Statement> statements) {
        this.statements = statements;
        this.context    = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_statement_list, parent, false);
        return new StatementHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            final StatementHolder holder    = (StatementHolder) viewHolder;
            Statement statement             = statements.get(position);
            SimpleDateFormat sdf            = new SimpleDateFormat(Helper.STRING_DATE_INPUT_PATTERN);

            holder.titleTextView.setText(statement.getTitle());
            holder.descTextView.setText(statement.getDesc());
            holder.dateTextView.setText(new SimpleDateFormat(Helper.STRING_DATE_OUTPUT_PATTERN).format(Objects.requireNonNull(sdf.parse(statement.getDate()))));
            holder.valueTextView.setText(String.valueOf(statement.getValue()));
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return this.statements.size();
    }

    public class StatementHolder extends RecyclerView.ViewHolder{

        public TextView titleTextView;
        public TextView descTextView;
        public TextView dateTextView;
        public TextView valueTextView;

        public StatementHolder(View view) {
            super(view);

            this.titleTextView   = view.findViewById(R.id.item_statement_title_textView);
            this.descTextView    = view.findViewById(R.id.item_statement_desc_textView);
            this.dateTextView    = view.findViewById(R.id.item_statement_date_textView);
            this.valueTextView   = view.findViewById(R.id.item_statement_value_textView);
        }
    }
}
