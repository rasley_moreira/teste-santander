package br.com.testesantander.Presentation.Enums;

import androidx.fragment.app.Fragment;
import br.com.testesantander.Presentation.Fragments.Home.HomeFragment;
import br.com.testesantander.Presentation.Fragments.Login.LoginFragment;

public enum EFragmentManager {

    HOME    (HomeFragment.getInstance()),
    LOGIN   (LoginFragment.getInstance());

    private Fragment value;

    EFragmentManager(Fragment value){
        this.value = value;
    }

    public Fragment getValue(){
        return this.value;
    }
}
