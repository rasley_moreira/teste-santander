package br.com.testesantander.Presentation.Fragments.Home;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import br.com.testesantander.Activity.MainActivity;
import br.com.testesantander.Helper.Helper;
import br.com.testesantander.Model.APIResponse.Error;
import br.com.testesantander.Model.Statement.Statement;
import br.com.testesantander.Model.User.User;
import br.com.testesantander.Protocols.IDialogAction;
import br.com.testesantander.Protocols.IResponse;
import br.com.testesantander.R;
import br.com.testesantander.Repository.UserRepositoryImpl;
import br.com.testesantander.Presentation.Adapters.StatementAdapter;
import br.com.testesantander.Presentation.Enums.EFragmentManager;
import br.com.testesantander.Presentation.Fragments.Login.LoginFragment;
import br.com.testesantander.databinding.HomeFragmentBinding;

public class HomeFragment extends Fragment implements IResponse<List<Statement>>, IDialogAction {

    private static final String TAG = LoginFragment.class.getName();
    private static HomeFragment _instance;

    //UI
    private HomeFragmentBinding binding;

    //Models
    private List<Statement>     statements;
    private User                userSession;

    //Repository
    private UserRepositoryImpl  userRepository;

    public static HomeFragment getInstance() {
        if(HomeFragment._instance == null)
            return new HomeFragment();

        return HomeFragment._instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = HomeFragmentBinding.inflate(inflater);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.init();
    }

    private void init(){
        this.binding.logout.setOnClickListener(v -> {
            HomeFragment.this.logout();
        });

        this.loadUserData();
        this.loadDataFromRepository();
    }

    private void logout() {
        Helper.clearSession(Objects.requireNonNull(this.getContext()));
        MainActivity.fragmentManager(EFragmentManager.LOGIN);
    }

    private void loadUserData() {
        this.userSession    = Helper.getUserSession(Objects.requireNonNull(this.getContext()));
        Locale locale       = new Locale("pt", "BR");
        NumberFormat nf     = NumberFormat.getCurrencyInstance(locale);

        this.binding.name.setText(this.userSession.getName());
        this.binding.agency.setText(this.userSession.getBankAccount());
        this.binding.bankAccount.setText(Helper.simpleMaskAccount(this.userSession.getAgency()));
        this.binding.balance.setText(nf.format(this.userSession.getBalance()));
    }

    private void loadDataFromRepository() {
        this.userRepository = new UserRepositoryImpl();
        this.userRepository.getData(this.getContext(), this, this.userSession);
    }

    private void loadStatementListView() {
        StatementAdapter statementAdapter   = new StatementAdapter(this.getContext(), this.statements);
        RecyclerView.LayoutManager layout   = new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false);

        this.binding.statementList.setAdapter(statementAdapter);
        this.binding.statementList.setLayoutManager(layout);
    }

    @Override
    public void onSuccess(List<Statement> statements) {
        this.statements = statements;
        this.loadStatementListView();
    }

    @Override
    public void onError(Error response) {
        try {
            Log.d(TAG, "onError: " + response.getMessage());
            Helper.showDialog(this.getActivity(), this, getString(R.string.error_home_request_title), response.getMessage());
        }catch(Exception e){
            Log.d(TAG, "onFailure: " + e.getMessage());
        }
    }

    @Override
    public void onFailure(Error response) {
        try {
            Log.d(TAG, "onFailure: " + response.getMessage());
            Helper.showDialog(this.getActivity(), this, getString(R.string.internal_error_server), getString(R.string.internal_error_server_message));
        }catch(Exception e){
            Log.d(TAG, "onFailure: " + e.getMessage());
        }
    }

    @Override
    public void onClickPositiveDialogButton() {
        this.logout();
    }

    @Override
    public void onClickNegativeDialogButton() {
        this.logout();
    }
}
