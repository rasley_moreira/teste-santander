package br.com.testesantander.Presentation.Fragments.Login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import br.com.testesantander.Activity.MainActivity;
import br.com.testesantander.Helper.Helper;
import br.com.testesantander.Model.APIResponse.Error;
import br.com.testesantander.Model.User.User;
import br.com.testesantander.Protocols.IResponse;
import br.com.testesantander.R;
import br.com.testesantander.Repository.UserRepositoryImpl;
import br.com.testesantander.Presentation.Enums.EFragmentManager;
import br.com.testesantander.databinding.LoginFragmentBinding;

public class LoginFragment extends Fragment implements IResponse<User> {

    private static final String     TAG = LoginFragment.class.getName();
    private static LoginFragment    _instance;

    //UI
    private LoginFragmentBinding    binding;

    //Repository
    private UserRepositoryImpl userRepository;

    public static LoginFragment getInstance() {
        if(LoginFragment._instance == null)
            return new LoginFragment();

        return LoginFragment._instance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.binding = LoginFragmentBinding.inflate(inflater, container, false);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.init();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.binding.userLogin.setText("");
        this.binding.password.setText("");
    }

    private void init() {
        this.userRepository = new UserRepositoryImpl();
        this.verifySession();
        this.binding.loginButton.setOnClickListener(v -> LoginFragment.this.login());
    }

    private void verifySession() {
        User userSession = Helper.getUserSession(Objects.requireNonNull(this.getContext()));
        if(userSession != null){
            MainActivity.fragmentManager(EFragmentManager.HOME);
        }
    }

    public void login(){
        if(this.formValidate(this.binding.userLogin) || this.formValidate(this.binding.password)) {
            String user     = this.binding.userLogin.getText().toString();
            String password = this.binding.password.getText().toString();

            this.userRepository.login(this.getContext(), this, user, password);
        }else{
            Helper.showDialog(this.getActivity(), getString(R.string.error_login_title), getString(R.string.error_user_login_message));
        }
    }

    private boolean formValidate(EditText _editText){
        return !_editText.getText().toString().equals("");
    }

    @Override
    public void onSuccess(User user) {
        Helper.saveUserSession(Objects.requireNonNull(this.getContext()), user);
        MainActivity.fragmentManager(EFragmentManager.HOME);
    }

    @Override
    public void onError(Error response) {
        try {
            Log.d(TAG, "onError: " + response.getMessage());
            Helper.showDialog(this.getActivity(), getString(R.string.error_login_title), response.getMessage());
        }catch(Exception e){
            Log.d(TAG, "onFailure: " + e.getMessage());
        }
    }

    @Override
    public void onFailure(Error response) {
        try {
            Log.d(TAG, "onFailure: " + response.getMessage());
            Helper.showDialog(this.getActivity(), getString(R.string.internal_error_server), getString(R.string.internal_error_server_message));
        }catch(Exception e){
            Log.d(TAG, "onFailure: " + e.getMessage());
        }
    }
}
