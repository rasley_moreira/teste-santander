package br.com.testesantander.Protocols;

import br.com.testesantander.Model.APIResponse.UserResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface IAuthentication {

    @FormUrlEncoded
    @POST("login")
    public Call<UserResponse> requestLogin(@Field("user") String user, @Field("password") String password);
}
