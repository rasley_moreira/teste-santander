package br.com.testesantander.Protocols;

public interface IDialogAction {
    public void onClickPositiveDialogButton();
    public void onClickNegativeDialogButton();
}
