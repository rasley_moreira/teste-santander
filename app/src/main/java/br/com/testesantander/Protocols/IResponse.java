package br.com.testesantander.Protocols;

import br.com.testesantander.Model.APIResponse.Error;

public interface IResponse<T> {
    public void onSuccess(T response);
    public void onError(Error response);
    public void onFailure(Error response);
}
