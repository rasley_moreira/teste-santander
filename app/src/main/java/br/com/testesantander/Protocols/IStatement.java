package br.com.testesantander.Protocols;

import br.com.testesantander.Model.APIResponse.StatementResponse;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IStatement {

    @GET("statements/{user_id}")
    public Call<StatementResponse> requestStatements(@Path("user_id") Integer user_id);

}
