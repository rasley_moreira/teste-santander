package br.com.testesantander.Protocols;

import android.content.Context;

import br.com.testesantander.Model.User.User;

public interface IUserRepository {

    public void login(Context context, IResponse response, String user, String password);
    public void getData(Context context, IResponse response, User user);

}
