package br.com.testesantander.Repository;

import android.content.Context;

import br.com.testesantander.Api.Tasks.AuthenticationResponseTask;
import br.com.testesantander.Api.Tasks.StatementResponseTask;
import br.com.testesantander.Model.User.User;
import br.com.testesantander.Protocols.IResponse;
import br.com.testesantander.Protocols.IUserRepository;

public class UserRepositoryImpl implements IUserRepository {

    @Override
    public void login(Context context, IResponse response, String user, String password) {
        new AuthenticationResponseTask(context, response).execute(user, password);
    }

    @Override
    public void getData(Context context, IResponse response, User user) {
        new StatementResponseTask(context, response, user).execute();
    }

}
